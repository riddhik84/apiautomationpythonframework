from rest_apis.search import Search

'''
Rest APIs class to generate objects for each github API class.
Example: github search API
'''


class Apis:
    def __init__(self):
        print("Apis")

    def search(self):
        return Search()
