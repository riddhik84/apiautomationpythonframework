from rest_apis.rest_base import *

BASE_URL_SEARCH = BASE_API_URL + 'search/repositories?q='

'''
Github search API class
'''


class Search(RestBase):
    def __init__(self):
        RestBase.__init__(self)

    '''
        Github search repository API
    '''

    def search_repository(self, query, sort=None, order=None):
        """
        :param query: query string
        :param sort: sort by (stars)
        :param order: asc or desc order
        :return: response code
        """
        query = BASE_URL_SEARCH + query
        if sort:
            query = query + '&sort=' + sort
        if order:
            query = query + '&order=' + order

        response = self.http_get(query)
        return response
