import requests
import json
import sys
import time
from logger import logger as logger
from helper.HTTPResponseCode import HTTPResponseCode

BASE_API_URL = 'https://api.github.com/' # github api base URL

'''
RestBase class for http request methods like GET, PUT, POST etc.
'''


class RestBase:
    def __init__(self):
        print("RestBase")

    '''
        http GET method
    '''

    def http_get(self, url, payload=None, timeout=0, stream=False):
        """
            :param url: api url
            :param payload: api palyload
            :param timeout: api timeout
            :param stream: api stream
        """

        header_info = None
        response = None
        RETRY_TIMES = 5
        connection_failure_retries = 0

        while response is None and connection_failure_retries < RETRY_TIMES:
            try:
                response = requests.get(url, payload, headers=header_info, stream=stream)
                if timeout > 0:
                    start_time = time.time()
                    while time.time() < start_time + timeout:
                        response = requests.get(url, payload, headers=header_info, stream=stream)
                        if response.status_code >= HTTPResponseCode.BAD_REQUEST:
                            break
                        if response.status_code >= HTTPResponseCode.SERVER_ERROR:
                            break
                        else:
                            time.sleep(1)

            except requests.ConnectionError as e:
                connection_failure_retries += 1
                time.sleep(5)
                logger.exception("Connection Error: " + str(e))
                continue
            except requests.RequestException as e:
                logger.exception(str(e))
                break

        logger.info("response_code={0}".format(response))
        return response
