import time


def print_logs(response, error_msg):
    print(response.status_code + '\n' + response.content + '\n' + error_msg)


def current_timestamp():
    return int(time.time())