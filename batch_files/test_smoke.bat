:: change to the folder where this script file is located
cd /d "%~dp0"

:: change to parent folder
cd ..

rmdir "./report" /S /Q

:: run smoke tests
py.test -vv --categories "suite=smoke" --maxfail=100 --html="./report/pytr_html.html" --alluredir "./report"

:: generate allure report
allure generate ./report --clean

:: open allure report
allure open