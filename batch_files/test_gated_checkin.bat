SET PATH=C:\Python36;C:\Python36\Scripts;%PATH%

:: change to the folder where this script file is located
cd /d "%~dp0"

:: change to parent folder
cd ..

:: run tests for gated checkin
py.test -vv --maxfail=100 --html="./report/pytr_html.html"