import pytest
from rest_apis import apis
from test_suit.test_github_search.search_test_data import *
from helper.HTTPResponseCode import *
from helper.utils import *

test_apis = apis.Apis()
test_search_apis = test_apis.search()


# Github search API tests
class TestGithubSearch:

    # Search repository api test with positive keywords
    @pytest.mark.categories(component='github_search', suite='smoke')
    @pytest.mark.parametrize('keyword', searchkeywords_positive)
    def test_gihub_search_positive(self, keyword):
        response = test_search_apis.search_repository(keyword)
        assert response.status_code == HTTPResponseCode.OK, print_logs(response, "Response code mismatch")

    # Search repository api test with special case keywords
    @pytest.mark.categories(component='github_search')
    @pytest.mark.parametrize('keyword', searchkeywords_special)
    def test_gihub_search_special(self, keyword):
        response = test_search_apis.search_repository(keyword)
        assert response.status_code == HTTPResponseCode.OK, print_logs(response, "Response code mismatch")

    # Search repository api test with negative test keywords
    @pytest.mark.parametrize('keyword', searchkeywords_negative)
    @pytest.mark.categories(component='github_search')
    def test_gihub_search_negative(self, keyword):
        response = test_search_apis.search_repository(keyword)
        assert response.status_code == HTTPResponseCode.UNPROCESSABLE_ENTITY, print_logs(response,
                                                                                         "Response code mismatch")
