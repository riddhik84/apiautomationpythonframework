# API Automation Python Framework

## Requirements
* [python 3.6](https://www.python.org/downloads/release/python-360/)
* [pytest](https://docs.pytest.org/en/latest/)
* [requests](https://pypi.org/project/requests/)
* [allure](https://pypi.org/project/allure-pytest/)

## Installation
Execute script:
```
apiautomationpythonframework\prepare_environment.sh
```

## API Service
Github APIs are used for framework implementation.
* [Github](https://developer.github.com/v3/)


## Test Execution
Execute script:
```bash
apiautomationpythonframework\batch_files\test_integration.bat
```

## Run tests by Category
Run smoke tests
```
apiautomationpythonframework\batch_files\test_smoke.bat
```


## Reporting
Allure is used to generate reports.
```bash
apiautomationpythonframework\batch_files\allure_report.bat
```

To generate allure report locally in windows, install scoop from PowerShell.
```
iex (new-object net.webclient).downloadstring('https://get.scoop.sh')

scoop install allure
```